// wrappers for dispatching actions

const incrementAction = (payload) => {
	return {
		type: 'INC',
		payload: payload
	};
};

const decrementAction = (payload) => {
	return {
		type: 'DEC',
		payload: payload
	};
};

export {
	incrementAction,
	decrementAction
};