// counterReducer receives the current state and the dispatched action
// and returns a new state object updated to reflect the changes
// from the action.
const counterReducer = (state = { count: 0 }, action) => {
	let result = state;
	switch(action.type) {
		case 'INC':
			result = Object.assign({}, {
				count: state.count + action.payload
			});
			break;
		case 'DEC':
			result = Object.assign({}, {
				count: state.count - action.payload
			});
			break;
		default:
			break
	}

	return result;
};

export default counterReducer;