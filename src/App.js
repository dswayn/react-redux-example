import React from 'react';
import { connect } from 'react-redux'

import { incrementAction, decrementAction } from './actions/counterActions';
import './App.css';

// define our standard react component
const AppComponent = (props) => {
  return (
    <div>
      <h1>Counter is {props.count}</h1>
      <button onClick={props.onIncrementClick}>Increment</button>
      <button onClick={props.onDecrementClick}>Decrement</button>
    </div>
  );
};

// take the count value from the global state object and pass it as a property
const mapStateToProps = state => {
  return {
    count: state.count
  };
};

// define event dispatchers auto-hooked to the store
const mapDispatchToProps = dispatch => {
  return {
    onIncrementClick: () => { dispatch(incrementAction(1)); },
    onDecrementClick: () => { dispatch(decrementAction(1)); }
  };
};

// connect generates a new version of the react component based upon the above functions
const App = connect(
  mapStateToProps,
  mapDispatchToProps
)(AppComponent);

export default App;